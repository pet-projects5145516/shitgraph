using System.Text.RegularExpressions;

public class UI
{

    static string num_pattern = @"^(?<number>[-+]?[0-9]+(\.[0-9]+)?([eE][-+]?[0-9]+)?)$";
    static string[] key_words = { "IF", "THEN", "ELSIF", "END", "OR", "NOT", "AND", };


    public UI(string[] args)
    {
        if (args.Length == 0)
        {
            Console.WriteLine(HelloWorld);
            Console.WriteLine("Введите строку для проверки:");

            string line_to_chech = Console.ReadLine();

            var r = Parser.GetReport(line_to_chech);

            var view = GetNiceView(r);
            Console.WriteLine(view);
        }
        else
        {
            foreach (var line in args)
            {
                Console.WriteLine("-----------------------------------------");
                Console.WriteLine(line);
                var r = Parser.GetReport(line);

                var view = GetNiceView(r);
                Console.WriteLine(view);
            }

        }

    }

    private static string HelloWorld = "Анализатор строк. \nВыполнила  Карян Л.Д. Вариант №16";





    private static string GetNiceView(Report r)
    {
        List<string> lines = new List<string>();

        string input_info = $"Входная строка: {r.InputLine}";
        string status_line =
                    r.IsSuccess() ?
                    "Строка соответсвует граматике" :
                    "Строка не соответсвует граматике";

        //lines.Add(input_info);

        lines.Add(status_line);

        if (r.Error.HasValue)
        {
            var err = r.Error.Value;
            string error_line =
                string.Join("", Enumerable.Repeat(" ", err.Pointer)) + '^';
            lines.Add(r.InputLine);
            lines.Add(error_line);
            lines.Add($"Состояние: {r.StatesLog.Last()}");
            //lines.Add(string.Join("->", r.States));
        }
        else
        {
            var sf = GetSemanticReport(r.InputLine);
            SemanticType[] toprint = { SemanticType.Number, SemanticType.ID };

            foreach (SemanticType type in toprint)
            {
                var names = sf.Where(s => s.Type == type).Select(s => s.Text);
                if (names.Any())
                {
                    if (type == SemanticType.ID)
                    {
                        lines.Add("IDS:");
                        foreach (string name in names)
                        {
                            string report = name;
                            if (name.Length > 8) report += " Длина ID должно быть небольше 8 знаков!";
                            lines.Add(report);
                        }

                    }
                    else if (type == SemanticType.Number)
                    {
                        lines.Add("Numbers:");
                        foreach (string name in names)
                            lines.Add(GetNumberReport(name));
                    }
                }
                lines.Add("\n");
            }

        }

        return string.Join('\n', lines.ToArray());
    }


    private static string GetNumberReport(string num)
    {
        string report = "";


        bool is_integer, is_float, is_decimal;

        is_decimal = Regex.Match(num.Trim(), @"^[-+]?[0-9]+\.[0-9]+[eE][-+]?[0-9]+$").Success;
        is_integer = Regex.Match(num.Trim(), @"^[-+]?[0-9]+$").Success;
        is_float = Regex.Match(num, @"^[-+]?[0-9]+\.[0-9]+$").Success;

        if (is_integer)
        {

            int number;
            bool succes = int.TryParse(num, out number);

            if (succes)
            {
                bool in_range = (number < short.MaxValue) & (number > short.MinValue) ? true : false;
                report = $"Целое число: {number}";
                if (!in_range) report += " Вне диапазона допустимых значений!";
            }
            else
            {
                report = $"Слишком большое число {num}";
            }
        }
        else if (is_decimal)
        {
            report = $"Число с фиксированной точкой: {num}";
            var pices = num.Split('E', 'e');
            if (pices[0].Length > 15) report += "Слишком много чисел в мантисе";
        }
        else if (is_float) report = $"Число с плавающей точкой: {num}";
        else report = $"Неизвестный тип: {num}";


        return report;
    }


    private static List<SemanticFragment> GetSemanticReport(string line)
    {
        string[] fragments = line.Split(' ', '(', ')', '=', '>', '<', ':', ';', '[',']');

        List<SemanticFragment> semanticFragments = new List<SemanticFragment>();

        foreach (string fragment in fragments)
        {
            if (!string.IsNullOrWhiteSpace(fragment))
            {
                SemanticType type = SemanticType.Unknown;
                Match num_match = Regex.Match(fragment.Trim(), num_pattern);

                if (num_match.Success) type = SemanticType.Number;
                else if (key_words.Contains(fragment)) type = SemanticType.KeyWord;
                else type = SemanticType.ID;

                if ( !semanticFragments.Where(f => f.Text == fragment.Trim()).Any())
                {
                    semanticFragments.Add(new SemanticFragment() { Text = fragment.Trim(), Type = type });
                }
            }
        }

        return semanticFragments;
    }

    public struct SemanticFragment
    {
        public string Text { get; init; }
        public SemanticType Type { get; init; }
    }


    public enum SemanticType
    {
        Number, ID, KeyWord, Unknown
    }

}
