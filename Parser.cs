


public static class Parser
{

    public static Report GetReport(string line)
    {
        Error? error = null;
        List<State> StateLog = new List<State>();
        int pointer = -1;
        char current_char;


        State current_state = State.S;
        bool Stop = false;

        while ((pointer < line.Length - 1) && (current_state != State.F) && !Stop)
        {
            pointer++;
            StateLog.Add(current_state);
            current_char = line[pointer];
            switch (current_state)
            {
                case State.S:
                    if (current_char == 'I') { current_state = State.S1; }
                    else { Stop = true; }
                    break;
                case State.S1:
                    if (current_char == 'F') { current_state = State.S2; }
                    else { Stop = true; }
                    break;
                case State.S2:
                    if (current_char == ' ') { current_state = State.S3; }
                    else { Stop = true; }
                    break;
                case State.S3:
                    if (current_char == ' ') { current_state = State.S3; }
                    else if (Char.IsLetter(current_char)) { current_state = State.A1; }
                    else if (current_char == '_') { current_state = State.A1; }
                    else if (current_char == '-') { current_state = State.B1; }
                    else if (current_char == '1') { current_state = State.B2; }
                    else if (current_char == '2') { current_state = State.B2; }
                    else if (current_char == '3') { current_state = State.B2; }
                    else if (current_char == '4') { current_state = State.B2; }
                    else if (current_char == '5') { current_state = State.B2; }
                    else if (current_char == '6') { current_state = State.B2; }
                    else if (current_char == '7') { current_state = State.B2; }
                    else if (current_char == '8') { current_state = State.B2; }
                    else if (current_char == '9') { current_state = State.B2; }
                    else if (current_char == '0') { current_state = State.B3; }
                    else { Stop = true; }
                    break;
                case State.A1:
                    if (Char.IsLetter(current_char)) { current_state = State.A1; }
                    else if (current_char == '1') { current_state = State.A1; }
                    else if (current_char == '2') { current_state = State.A1; }
                    else if (current_char == '3') { current_state = State.A1; }
                    else if (current_char == '4') { current_state = State.A1; }
                    else if (current_char == '5') { current_state = State.A1; }
                    else if (current_char == '6') { current_state = State.A1; }
                    else if (current_char == '7') { current_state = State.A1; }
                    else if (current_char == '8') { current_state = State.A1; }
                    else if (current_char == '9') { current_state = State.A1; }
                    else if (current_char == '_') { current_state = State.A1; }
                    else if (current_char == '[') { current_state = State.A2; }
                    else if (current_char == ' ') { current_state = State.A4; }
                    else if (current_char == '=') { current_state = State.C1; }
                    else if (current_char == '<') { current_state = State.C2; }
                    else if (current_char == '>') { current_state = State.C3; }
                    else if (current_char == '#') { current_state = State.C7; }
                    else if (current_char == '&') { current_state = State.C8; }
                    else if (current_char == '~') { current_state = State.C9; }
                    else { Stop = true; }
                    break;
                case State.A2:
                    if (Char.IsLetter(current_char)) { current_state = State.A21; }
                    else if (current_char == '_') { current_state = State.A21; }
                    else if (current_char == '-') { current_state = State.A30; }
                    else if (current_char == '1') { current_state = State.A31; }
                    else if (current_char == '2') { current_state = State.A31; }
                    else if (current_char == '3') { current_state = State.A31; }
                    else if (current_char == '4') { current_state = State.A31; }
                    else if (current_char == '5') { current_state = State.A31; }
                    else if (current_char == '6') { current_state = State.A31; }
                    else if (current_char == '7') { current_state = State.A31; }
                    else if (current_char == '8') { current_state = State.A31; }
                    else if (current_char == '9') { current_state = State.A31; }
                    else if (current_char == '0') { current_state = State.A32; }
                    else { Stop = true; }
                    break;
                case State.A4:
                    if (current_char == ' ') { current_state = State.A4; }
                    else if (current_char == '[') { current_state = State.A2; }
                    else if (current_char == 'T') { current_state = State.D1; }
                    else if (current_char == '=') { current_state = State.C1; }
                    else if (current_char == '<') { current_state = State.C2; }
                    else if (current_char == '>') { current_state = State.C3; }
                    else if (current_char == 'O') { current_state = State.C4; }
                    else if (current_char == 'N') { current_state = State.C5; }
                    else if (current_char == 'A') { current_state = State.C6; }
                    else if (current_char == '#') { current_state = State.C7; }
                    else if (current_char == '&') { current_state = State.C8; }
                    else if (current_char == '~') { current_state = State.C9; }
                    else { Stop = true; }
                    break;
                case State.A21:
                    if (Char.IsLetter(current_char)) { current_state = State.A21; }
                    else if (current_char == '1') { current_state = State.A21; }
                    else if (current_char == '2') { current_state = State.A21; }
                    else if (current_char == '3') { current_state = State.A21; }
                    else if (current_char == '4') { current_state = State.A21; }
                    else if (current_char == '5') { current_state = State.A21; }
                    else if (current_char == '6') { current_state = State.A21; }
                    else if (current_char == '7') { current_state = State.A21; }
                    else if (current_char == '8') { current_state = State.A21; }
                    else if (current_char == '9') { current_state = State.A21; }
                    else if (current_char == '_') { current_state = State.A21; }
                    else if (current_char == ']') { current_state = State.C; }
                    else { Stop = true; }
                    break;
                case State.C:
                    if (current_char == ' ') { current_state = State.C; }
                    else if (current_char == '=') { current_state = State.C1; }
                    else if (current_char == '<') { current_state = State.C2; }
                    else if (current_char == '>') { current_state = State.C3; }
                    else if (current_char == 'O') { current_state = State.C4; }
                    else if (current_char == 'N') { current_state = State.C5; }
                    else if (current_char == 'A') { current_state = State.C6; }
                    else if (current_char == '#') { current_state = State.C7; }
                    else if (current_char == '&') { current_state = State.C8; }
                    else if (current_char == '~') { current_state = State.C9; }
                    else if (current_char == 'T') { current_state = State.D1; }
                    else { Stop = true; }
                    break;
                case State.A30:
                    if (current_char == '1') { current_state = State.A31; }
                    else if (current_char == '2') { current_state = State.A31; }
                    else if (current_char == '3') { current_state = State.A31; }
                    else if (current_char == '4') { current_state = State.A31; }
                    else if (current_char == '5') { current_state = State.A31; }
                    else if (current_char == '6') { current_state = State.A31; }
                    else if (current_char == '7') { current_state = State.A31; }
                    else if (current_char == '8') { current_state = State.A31; }
                    else if (current_char == '9') { current_state = State.A31; }
                    else { Stop = true; }
                    break;
                case State.A31:
                    if (Char.IsDigit(current_char)) { current_state = State.A31; }
                    else if (current_char == ']') { current_state = State.C; }
                    else { Stop = true; }
                    break;
                case State.A32:
                    if (current_char == ']') { current_state = State.C; }
                    else { Stop = true; }
                    break;
                case State.C1:
                    if (current_char == ' ') { current_state = State.C0; }
                    else if (Char.IsLetter(current_char)) { current_state = State.C10; }
                    else if (current_char == '_') { current_state = State.C10; }
                    else if (current_char == '-') { current_state = State.C40; }
                    else if (current_char == '1') { current_state = State.C41; }
                    else if (current_char == '2') { current_state = State.C41; }
                    else if (current_char == '3') { current_state = State.C41; }
                    else if (current_char == '4') { current_state = State.C41; }
                    else if (current_char == '5') { current_state = State.C41; }
                    else if (current_char == '6') { current_state = State.C41; }
                    else if (current_char == '7') { current_state = State.C41; }
                    else if (current_char == '8') { current_state = State.C41; }
                    else if (current_char == '9') { current_state = State.C41; }
                    else if (current_char == '0') { current_state = State.C42; }
                    else { Stop = true; }
                    break;
                case State.C2:
                    if (current_char == '=') { current_state = State.C02; }
                    else if (current_char == '>') { current_state = State.C03; }
                    else if (current_char == ' ') { current_state = State.C0; }
                    else if (Char.IsLetter(current_char)) { current_state = State.C10; }
                    else if (current_char == '_') { current_state = State.C10; }
                    else if (current_char == '-') { current_state = State.C40; }
                    else if (current_char == '1') { current_state = State.C41; }
                    else if (current_char == '2') { current_state = State.C41; }
                    else if (current_char == '3') { current_state = State.C41; }
                    else if (current_char == '4') { current_state = State.C41; }
                    else if (current_char == '5') { current_state = State.C41; }
                    else if (current_char == '6') { current_state = State.C41; }
                    else if (current_char == '7') { current_state = State.C41; }
                    else if (current_char == '8') { current_state = State.C41; }
                    else if (current_char == '9') { current_state = State.C41; }
                    else if (current_char == '0') { current_state = State.C42; }
                    else { Stop = true; }
                    break;
                case State.C02:
                    if (current_char == ' ') { current_state = State.C0; }
                    else if (Char.IsLetter(current_char)) { current_state = State.C10; }
                    else if (current_char == '_') { current_state = State.C10; }
                    else if (current_char == '-') { current_state = State.C40; }
                    else if (current_char == '1') { current_state = State.C41; }
                    else if (current_char == '2') { current_state = State.C41; }
                    else if (current_char == '3') { current_state = State.C41; }
                    else if (current_char == '4') { current_state = State.C41; }
                    else if (current_char == '5') { current_state = State.C41; }
                    else if (current_char == '6') { current_state = State.C41; }
                    else if (current_char == '7') { current_state = State.C41; }
                    else if (current_char == '8') { current_state = State.C41; }
                    else if (current_char == '9') { current_state = State.C41; }
                    else if (current_char == '0') { current_state = State.C42; }
                    else { Stop = true; }
                    break;
                case State.C03:
                    if (current_char == ' ') { current_state = State.C0; }
                    else if (Char.IsLetter(current_char)) { current_state = State.C10; }
                    else if (current_char == '_') { current_state = State.C10; }
                    else if (current_char == '-') { current_state = State.C40; }
                    else if (current_char == '1') { current_state = State.C41; }
                    else if (current_char == '2') { current_state = State.C41; }
                    else if (current_char == '3') { current_state = State.C41; }
                    else if (current_char == '4') { current_state = State.C41; }
                    else if (current_char == '5') { current_state = State.C41; }
                    else if (current_char == '6') { current_state = State.C41; }
                    else if (current_char == '7') { current_state = State.C41; }
                    else if (current_char == '8') { current_state = State.C41; }
                    else if (current_char == '9') { current_state = State.C41; }
                    else if (current_char == '0') { current_state = State.C42; }
                    else { Stop = true; }
                    break;
                case State.C3:
                    if (current_char == '=') { current_state = State.C04; }
                    else if (current_char == ' ') { current_state = State.C0; }
                    else if (Char.IsLetter(current_char)) { current_state = State.C10; }
                    else if (current_char == '_') { current_state = State.C10; }
                    else if (current_char == '-') { current_state = State.C40; }
                    else if (current_char == '1') { current_state = State.C41; }
                    else if (current_char == '2') { current_state = State.C41; }
                    else if (current_char == '3') { current_state = State.C41; }
                    else if (current_char == '4') { current_state = State.C41; }
                    else if (current_char == '5') { current_state = State.C41; }
                    else if (current_char == '6') { current_state = State.C41; }
                    else if (current_char == '7') { current_state = State.C41; }
                    else if (current_char == '8') { current_state = State.C41; }
                    else if (current_char == '9') { current_state = State.C41; }
                    else if (current_char == '0') { current_state = State.C42; }
                    else { Stop = true; }
                    break;
                case State.C04:
                    if (current_char == ' ') { current_state = State.C0; }
                    else if (current_char == ' ') { current_state = State.C0; }
                    else if (Char.IsLetter(current_char)) { current_state = State.C10; }
                    else if (current_char == '_') { current_state = State.C10; }
                    else if (current_char == '-') { current_state = State.C40; }
                    else if (current_char == '1') { current_state = State.C41; }
                    else if (current_char == '2') { current_state = State.C41; }
                    else if (current_char == '3') { current_state = State.C41; }
                    else if (current_char == '4') { current_state = State.C41; }
                    else if (current_char == '5') { current_state = State.C41; }
                    else if (current_char == '6') { current_state = State.C41; }
                    else if (current_char == '7') { current_state = State.C41; }
                    else if (current_char == '8') { current_state = State.C41; }
                    else if (current_char == '9') { current_state = State.C41; }
                    else if (current_char == '0') { current_state = State.C42; }
                    else { Stop = true; }
                    break;
                case State.C4:
                    if (current_char == 'R') { current_state = State.C04; }
                    else { Stop = true; }
                    break;
                case State.C5:
                    if (current_char == 'O') { current_state = State.C05; }
                    else { Stop = true; }
                    break;
                case State.C05:
                    if (current_char == 'T') { current_state = State.C005; }
                    else { Stop = true; }
                    break;
                case State.C005:
                    if (current_char == ' ') { current_state = State.C0; }
                    else { Stop = true; }
                    break;
                case State.C6:
                    if (current_char == 'N') { current_state = State.C06; }
                    else { Stop = true; }
                    break;
                case State.C06:
                    if (current_char == 'D') { current_state = State.C006; }
                    else { Stop = true; }
                    break;
                case State.C006:
                    if (current_char == ' ') { current_state = State.C0; }
                    else { Stop = true; }
                    break;
                case State.C7:
                    if (current_char == ' ') { current_state = State.C0; }
                    else if (Char.IsLetter(current_char)) { current_state = State.C10; }
                    else if (current_char == '_') { current_state = State.C10; }
                    else if (current_char == '-') { current_state = State.C40; }
                    else if (current_char == '1') { current_state = State.C41; }
                    else if (current_char == '2') { current_state = State.C41; }
                    else if (current_char == '3') { current_state = State.C41; }
                    else if (current_char == '4') { current_state = State.C41; }
                    else if (current_char == '5') { current_state = State.C41; }
                    else if (current_char == '6') { current_state = State.C41; }
                    else if (current_char == '7') { current_state = State.C41; }
                    else if (current_char == '8') { current_state = State.C41; }
                    else if (current_char == '9') { current_state = State.C41; }
                    else if (current_char == '0') { current_state = State.C42; }
                    else { Stop = true; }
                    break;
                case State.C8:
                    if (current_char == ' ') { current_state = State.C0; }
                    else if (Char.IsLetter(current_char)) { current_state = State.C10; }
                    else if (current_char == '_') { current_state = State.C10; }
                    else if (current_char == '-') { current_state = State.C40; }
                    else if (current_char == '1') { current_state = State.C41; }
                    else if (current_char == '2') { current_state = State.C41; }
                    else if (current_char == '3') { current_state = State.C41; }
                    else if (current_char == '4') { current_state = State.C41; }
                    else if (current_char == '5') { current_state = State.C41; }
                    else if (current_char == '6') { current_state = State.C41; }
                    else if (current_char == '7') { current_state = State.C41; }
                    else if (current_char == '8') { current_state = State.C41; }
                    else if (current_char == '9') { current_state = State.C41; }
                    else if (current_char == '0') { current_state = State.C42; }
                    else { Stop = true; }
                    break;
                case State.C9:
                    if (current_char == ' ') { current_state = State.C0; }
                    else if (Char.IsLetter(current_char)) { current_state = State.C10; }
                    else if (current_char == '_') { current_state = State.C10; }
                    else if (current_char == '-') { current_state = State.C40; }
                    else if (current_char == '1') { current_state = State.C41; }
                    else if (current_char == '2') { current_state = State.C41; }
                    else if (current_char == '3') { current_state = State.C41; }
                    else if (current_char == '4') { current_state = State.C41; }
                    else if (current_char == '5') { current_state = State.C41; }
                    else if (current_char == '6') { current_state = State.C41; }
                    else if (current_char == '7') { current_state = State.C41; }
                    else if (current_char == '8') { current_state = State.C41; }
                    else if (current_char == '9') { current_state = State.C41; }
                    else if (current_char == '0') { current_state = State.C42; }
                    else { Stop = true; }
                    break;
                case State.D1:
                    if (current_char == 'H') { current_state = State.D2; }
                    else { Stop = true; }
                    break;
                case State.D2:
                    if (current_char == 'E') { current_state = State.D3; }
                    else { Stop = true; }
                    break;
                case State.D3:
                    if (current_char == 'N') { current_state = State.D4; }
                    else { Stop = true; }
                    break;
                case State.D4:
                    if (current_char == ' ') { current_state = State.D5; }
                    else { Stop = true; }
                    break;
                case State.C0:
                    if (current_char == ' ') { current_state = State.C0; }
                    else if (Char.IsLetter(current_char)) { current_state = State.C10; }
                    else if (current_char == '_') { current_state = State.C10; }
                    else if (current_char == '-') { current_state = State.C40; }
                    else if (current_char == '1') { current_state = State.C41; }
                    else if (current_char == '2') { current_state = State.C41; }
                    else if (current_char == '3') { current_state = State.C41; }
                    else if (current_char == '4') { current_state = State.C41; }
                    else if (current_char == '5') { current_state = State.C41; }
                    else if (current_char == '6') { current_state = State.C41; }
                    else if (current_char == '7') { current_state = State.C41; }
                    else if (current_char == '8') { current_state = State.C41; }
                    else if (current_char == '9') { current_state = State.C41; }
                    else if (current_char == '0') { current_state = State.C42; }
                    else { Stop = true; }
                    break;
                case State.C10:
                    if (Char.IsLetter(current_char)) { current_state = State.C10; }
                    else if (current_char == '1') { current_state = State.C10; }
                    else if (current_char == '2') { current_state = State.C10; }
                    else if (current_char == '3') { current_state = State.C10; }
                    else if (current_char == '4') { current_state = State.C10; }
                    else if (current_char == '5') { current_state = State.C10; }
                    else if (current_char == '6') { current_state = State.C10; }
                    else if (current_char == '7') { current_state = State.C10; }
                    else if (current_char == '8') { current_state = State.C10; }
                    else if (current_char == '9') { current_state = State.C10; }
                    else if (current_char == '_') { current_state = State.C10; }
                    else if (current_char == ' ') { current_state = State.C11; }
                    else if (current_char == '[') { current_state = State.C21; }
                    else { Stop = true; }
                    break;
                case State.C11:
                    if (current_char == ' ') { current_state = State.C11; }
                    else if (current_char == 'T') { current_state = State.D1; }
                    else if (current_char == '[') { current_state = State.C21; }
                    else { Stop = true; }
                    break;
                case State.C21:
                    if (Char.IsLetter(current_char)) { current_state = State.C22; }
                    else if (current_char == '_') { current_state = State.C22; }
                    else if (current_char == '-') { current_state = State.C30; }
                    else if (current_char == '1') { current_state = State.C31; }
                    else if (current_char == '2') { current_state = State.C31; }
                    else if (current_char == '3') { current_state = State.C31; }
                    else if (current_char == '4') { current_state = State.C31; }
                    else if (current_char == '5') { current_state = State.C31; }
                    else if (current_char == '6') { current_state = State.C31; }
                    else if (current_char == '7') { current_state = State.C31; }
                    else if (current_char == '8') { current_state = State.C31; }
                    else if (current_char == '9') { current_state = State.C31; }
                    else if (current_char == '0') { current_state = State.C32; }
                    else { Stop = true; }
                    break;
                case State.C22:
                    if (Char.IsLetter(current_char)) { current_state = State.C22; }
                    else if (current_char == '1') { current_state = State.C22; }
                    else if (current_char == '2') { current_state = State.C22; }
                    else if (current_char == '3') { current_state = State.C22; }
                    else if (current_char == '4') { current_state = State.C22; }
                    else if (current_char == '5') { current_state = State.C22; }
                    else if (current_char == '6') { current_state = State.C22; }
                    else if (current_char == '7') { current_state = State.C22; }
                    else if (current_char == '8') { current_state = State.C22; }
                    else if (current_char == '9') { current_state = State.C22; }
                    else if (current_char == '_') { current_state = State.C22; }
                    else if (current_char == ']') { current_state = State.C23; }
                    else { Stop = true; }
                    break;
                case State.C23:
                    if (current_char == ' ') { current_state = State.C24; }
                    else if (current_char == 'T') { current_state = State.D1; }
                    else { Stop = true; }
                    break;
                case State.C24:
                    if (current_char == ' ') { current_state = State.C24; }
                    else if (current_char == 'T') { current_state = State.D1; }
                    else { Stop = true; }
                    break;
                case State.C30:
                    if (current_char == '1') { current_state = State.C31; }
                    else if (current_char == '2') { current_state = State.C31; }
                    else if (current_char == '3') { current_state = State.C31; }
                    else if (current_char == '4') { current_state = State.C31; }
                    else if (current_char == '5') { current_state = State.C31; }
                    else if (current_char == '6') { current_state = State.C31; }
                    else if (current_char == '7') { current_state = State.C31; }
                    else if (current_char == '8') { current_state = State.C31; }
                    else if (current_char == '9') { current_state = State.C31; }
                    else { Stop = true; }
                    break;
                case State.C31:
                    if (Char.IsDigit(current_char)) { current_state = State.C31; }
                    else if (current_char == ']') { current_state = State.C33; }
                    else { Stop = true; }
                    break;
                case State.C32:
                    if (current_char == ']') { current_state = State.C33; }
                    else { Stop = true; }
                    break;
                case State.C33:
                    if (current_char == ' ') { current_state = State.C24; }
                    else if (current_char == 'T') { current_state = State.D1; }
                    else { Stop = true; }
                    break;
                case State.C40:
                    if (current_char == '1') { current_state = State.C41; }
                    else if (current_char == '2') { current_state = State.C41; }
                    else if (current_char == '3') { current_state = State.C41; }
                    else if (current_char == '4') { current_state = State.C41; }
                    else if (current_char == '5') { current_state = State.C41; }
                    else if (current_char == '6') { current_state = State.C41; }
                    else if (current_char == '7') { current_state = State.C41; }
                    else if (current_char == '8') { current_state = State.C41; }
                    else if (current_char == '9') { current_state = State.C41; }
                    else { Stop = true; }
                    break;
                case State.C41:
                    if (Char.IsDigit(current_char)) { current_state = State.C41; }
                    else if (current_char == ' ') { current_state = State.C43; }
                    else if (current_char == '.') { current_state = State.C44; }
                    else { Stop = true; }
                    break;
                case State.C42:
                    if (current_char == ' ') { current_state = State.C43; }
                    else if (current_char == '.') { current_state = State.C44; }
                    else { Stop = true; }
                    break;
                case State.C43:
                    if (current_char == ' ') { current_state = State.C43; }
                    else if (current_char == 'T') { current_state = State.D1; }
                    else { Stop = true; }
                    break;
                case State.C44:
                    if (Char.IsDigit(current_char)) { current_state = State.C48; }
                    else { Stop = true; }
                    break;
                case State.C48:
                    if (Char.IsDigit(current_char)) { current_state = State.C48; }
                    else if (current_char == ' ') { current_state = State.C43; }
                    else if (current_char == 'E') { current_state = State.C45; }
                    else { Stop = true; }
                    break;
                case State.C45:
                    if (current_char == '-') { current_state = State.C46; }
                    else if (current_char == '1') { current_state = State.C47; }
                    else if (current_char == '2') { current_state = State.C47; }
                    else if (current_char == '3') { current_state = State.C47; }
                    else if (current_char == '4') { current_state = State.C47; }
                    else if (current_char == '5') { current_state = State.C47; }
                    else if (current_char == '6') { current_state = State.C47; }
                    else if (current_char == '7') { current_state = State.C47; }
                    else if (current_char == '8') { current_state = State.C47; }
                    else if (current_char == '9') { current_state = State.C47; }
                    else { Stop = true; }
                    break;
                case State.C46:
                    if (current_char == '1') { current_state = State.C47; }
                    else if (current_char == '2') { current_state = State.C47; }
                    else if (current_char == '3') { current_state = State.C47; }
                    else if (current_char == '4') { current_state = State.C47; }
                    else if (current_char == '5') { current_state = State.C47; }
                    else if (current_char == '6') { current_state = State.C47; }
                    else if (current_char == '7') { current_state = State.C47; }
                    else if (current_char == '8') { current_state = State.C47; }
                    else if (current_char == '9') { current_state = State.C47; }
                    else { Stop = true; }
                    break;
                case State.C47:
                    if (Char.IsDigit(current_char)) { current_state = State.C47; }
                    else if (current_char == ' ') { current_state = State.C43; }
                    else { Stop = true; }
                    break;
                case State.B1:
                    if (current_char == '1') { current_state = State.B2; }
                    else if (current_char == '2') { current_state = State.B2; }
                    else if (current_char == '3') { current_state = State.B2; }
                    else if (current_char == '4') { current_state = State.B2; }
                    else if (current_char == '5') { current_state = State.B2; }
                    else if (current_char == '6') { current_state = State.B2; }
                    else if (current_char == '7') { current_state = State.B2; }
                    else if (current_char == '8') { current_state = State.B2; }
                    else if (current_char == '9') { current_state = State.B2; }
                    else { Stop = true; }
                    break;
                case State.B2:
                    if (Char.IsDigit(current_char)) { current_state = State.B2; }
                    else if (current_char == ' ') { current_state = State.B4; }
                    else if (current_char == '=') { current_state = State.C1; }
                    else if (current_char == '<') { current_state = State.C2; }
                    else if (current_char == '>') { current_state = State.C3; }
                    else if (current_char == 'O') { current_state = State.C4; }
                    else if (current_char == 'N') { current_state = State.C5; }
                    else if (current_char == 'A') { current_state = State.C6; }
                    else if (current_char == '#') { current_state = State.C7; }
                    else if (current_char == '&') { current_state = State.C8; }
                    else if (current_char == '~') { current_state = State.C9; }
                    else if (current_char == '.') { current_state = State.B5; }
                    else { Stop = true; }
                    break;
                case State.B3:
                    if (current_char == ' ') { current_state = State.B4; }
                    else if (current_char == '=') { current_state = State.C1; }
                    else if (current_char == '<') { current_state = State.C2; }
                    else if (current_char == '>') { current_state = State.C3; }
                    else if (current_char == 'O') { current_state = State.C4; }
                    else if (current_char == 'N') { current_state = State.C5; }
                    else if (current_char == 'A') { current_state = State.C6; }
                    else if (current_char == '#') { current_state = State.C7; }
                    else if (current_char == '&') { current_state = State.C8; }
                    else if (current_char == '~') { current_state = State.C9; }
                    else if (current_char == '.') { current_state = State.B5; }
                    else { Stop = true; }
                    break;
                case State.B4:
                    if (current_char == ' ') { current_state = State.B4; }
                    else if (current_char == 'T') { current_state = State.D1; }
                    else if (current_char == '=') { current_state = State.C1; }
                    else if (current_char == '<') { current_state = State.C2; }
                    else if (current_char == '>') { current_state = State.C3; }
                    else if (current_char == 'O') { current_state = State.C4; }
                    else if (current_char == 'N') { current_state = State.C5; }
                    else if (current_char == 'A') { current_state = State.C6; }
                    else if (current_char == '#') { current_state = State.C7; }
                    else if (current_char == '&') { current_state = State.C8; }
                    else if (current_char == '~') { current_state = State.C9; }
                    else { Stop = true; }
                    break;
                case State.B5:
                    if (Char.IsDigit(current_char)) { current_state = State.B6; }
                    else { Stop = true; }
                    break;
                case State.B6:
                    if (Char.IsDigit(current_char)) { current_state = State.B6; }
                    else if (current_char == ' ') { current_state = State.B4; }
                    else if (current_char == '=') { current_state = State.C1; }
                    else if (current_char == '<') { current_state = State.C2; }
                    else if (current_char == '>') { current_state = State.C3; }
                    else if (current_char == 'O') { current_state = State.C4; }
                    else if (current_char == 'N') { current_state = State.C5; }
                    else if (current_char == 'A') { current_state = State.C6; }
                    else if (current_char == '#') { current_state = State.C7; }
                    else if (current_char == '&') { current_state = State.C8; }
                    else if (current_char == '~') { current_state = State.C9; }
                    else if (current_char == 'E') { current_state = State.B7; }
                    else { Stop = true; }
                    break;
                case State.B7:
                    if (Char.IsDigit(current_char)) { current_state = State.B8; }
                    else { Stop = true; }
                    break;
                case State.B8:
                    if (Char.IsDigit(current_char)) { current_state = State.B8; }
                    else if (current_char == ' ') { current_state = State.B4; }
                    else if (current_char == '=') { current_state = State.C1; }
                    else if (current_char == '<') { current_state = State.C2; }
                    else if (current_char == '>') { current_state = State.C3; }
                    else if (current_char == 'O') { current_state = State.C4; }
                    else if (current_char == 'N') { current_state = State.C5; }
                    else if (current_char == 'A') { current_state = State.C6; }
                    else if (current_char == '#') { current_state = State.C7; }
                    else if (current_char == '&') { current_state = State.C8; }
                    else if (current_char == '~') { current_state = State.C9; }
                    else { Stop = true; }
                    break;
                case State.D5:
                    if (Char.IsLetter(current_char)) { current_state = State.D6; }
                    else if (current_char == '_') { current_state = State.D6; }
                    else { Stop = true; }
                    break;
                case State.D6:
                    if (Char.IsLetter(current_char)) { current_state = State.D6; }
                    else if (current_char == '_') { current_state = State.D6; }
                    else if (Char.IsDigit(current_char)) { current_state = State.D6; }
                    else if (current_char == ' ') { current_state = State.D7; }
                    else if (current_char == ':') { current_state = State.D10; }
                    else if (current_char == '[') { current_state = State.D20; }
                    else if (current_char == ':') { current_state = State.D10; }
                    else { Stop = true; }
                    break;
                case State.D7:
                    if (current_char == ' ') { current_state = State.D7; }
                    else if (current_char == ':') { current_state = State.D10; }
                    else if (current_char == '[') { current_state = State.D20; }
                    else if (current_char == ':') { current_state = State.D10; }
                    else { Stop = true; }
                    break;
                case State.D10:
                    if (current_char == '=') { current_state = State.D11; }
                    else { Stop = true; }
                    break;
                case State.D11:
                    if (Char.IsLetter(current_char)) { current_state = State.D12; }
                    else if (current_char == '_') { current_state = State.D12; }
                    else if (current_char == '-') { current_state = State.D29; }
                    else if (current_char == ' ') { current_state = State.D11; }
                    else if (current_char == '1') { current_state = State.D30; }
                    else if (current_char == '2') { current_state = State.D30; }
                    else if (current_char == '3') { current_state = State.D30; }
                    else if (current_char == '4') { current_state = State.D30; }
                    else if (current_char == '5') { current_state = State.D30; }
                    else if (current_char == '6') { current_state = State.D30; }
                    else if (current_char == '7') { current_state = State.D30; }
                    else if (current_char == '8') { current_state = State.D30; }
                    else if (current_char == '9') { current_state = State.D30; }
                    else if (current_char == '0') { current_state = State.D31; }
                    else { Stop = true; }
                    break;
                case State.D12:
                    if (Char.IsLetter(current_char)) { current_state = State.D12; }
                    else if (current_char == '_') { current_state = State.D12; }
                    else if (Char.IsDigit(current_char)) { current_state = State.D12; }
                    else if (current_char == ' ') { current_state = State.D14; }
                    else if (current_char == '[') { current_state = State.D13; }
                    else { Stop = true; }
                    break;
                case State.D14:
                    if (current_char == ' ') { current_state = State.D14; }
                    else if (current_char == 'E') { current_state = State.G1; }
                    else if (current_char == '[') { current_state = State.D13; }
                    else { Stop = true; }
                    break;
                case State.D13:
                    if (current_char == '-') { current_state = State.D15; }
                    else if (current_char == '1') { current_state = State.D16; }
                    else if (current_char == '2') { current_state = State.D16; }
                    else if (current_char == '3') { current_state = State.D16; }
                    else if (current_char == '4') { current_state = State.D16; }
                    else if (current_char == '5') { current_state = State.D16; }
                    else if (current_char == '6') { current_state = State.D16; }
                    else if (current_char == '7') { current_state = State.D16; }
                    else if (current_char == '8') { current_state = State.D16; }
                    else if (current_char == '9') { current_state = State.D16; }
                    else if (current_char == '0') { current_state = State.D17; }
                    else if (Char.IsLetter(current_char)) { current_state = State.D40; }
                    else if (current_char == '_') { current_state = State.D40; }
                    else { Stop = true; }
                    break;
                case State.D20:
                    if (Char.IsLetter(current_char)) { current_state = State.D21; }
                    else if (current_char == '_') { current_state = State.D21; }
                    else if (current_char == '-') { current_state = State.D24; }
                    else if (current_char == '1') { current_state = State.D25; }
                    else if (current_char == '2') { current_state = State.D25; }
                    else if (current_char == '3') { current_state = State.D25; }
                    else if (current_char == '4') { current_state = State.D25; }
                    else if (current_char == '5') { current_state = State.D25; }
                    else if (current_char == '6') { current_state = State.D25; }
                    else if (current_char == '7') { current_state = State.D25; }
                    else if (current_char == '8') { current_state = State.D25; }
                    else if (current_char == '9') { current_state = State.D25; }
                    else if (current_char == '0') { current_state = State.D26; }
                    else { Stop = true; }
                    break;
                case State.D21:
                    if (Char.IsLetter(current_char)) { current_state = State.D21; }
                    else if (current_char == '_') { current_state = State.D21; }
                    else if (Char.IsDigit(current_char)) { current_state = State.D21; }
                    else if (current_char == ']') { current_state = State.D22; }
                    else { Stop = true; }
                    break;
                case State.D22:
                    if (current_char == ' ') { current_state = State.D23; }
                    else if (current_char == ':') { current_state = State.D10; }
                    else { Stop = true; }
                    break;
                case State.D23:
                    if (current_char == ' ') { current_state = State.D23; }
                    else if (current_char == ':') { current_state = State.D10; }
                    else { Stop = true; }
                    break;
                case State.G1:
                    if (current_char == 'N') { current_state = State.G2; }
                    else if (current_char == 'L') { current_state = State.H1; }
                    else { Stop = true; }
                    break;
                case State.G2:
                    if (current_char == 'D') { current_state = State.G3; }
                    else { Stop = true; }
                    break;
                case State.G3:
                    if (current_char == ';') { current_state = State.F; }
                    else { Stop = true; }
                    break;
                case State.D24:
                    if (current_char == '1') { current_state = State.D25; }
                    else if (current_char == '2') { current_state = State.D25; }
                    else if (current_char == '3') { current_state = State.D25; }
                    else if (current_char == '4') { current_state = State.D25; }
                    else if (current_char == '5') { current_state = State.D25; }
                    else if (current_char == '6') { current_state = State.D25; }
                    else if (current_char == '7') { current_state = State.D25; }
                    else if (current_char == '8') { current_state = State.D25; }
                    else if (current_char == '9') { current_state = State.D25; }
                    else { Stop = true; }
                    break;
                case State.D25:
                    if (Char.IsDigit(current_char)) { current_state = State.D25; }
                    else if (current_char == ']') { current_state = State.D27; }
                    else { Stop = true; }
                    break;
                case State.D26:
                    if (current_char == ']') { current_state = State.D27; }
                    else { Stop = true; }
                    break;
                case State.D27:
                    if (current_char == ' ') { current_state = State.D28; }
                    else if (current_char == ':') { current_state = State.D10; }
                    else { Stop = true; }
                    break;
                case State.D28:
                    if (current_char == ' ') { current_state = State.D28; }
                    else if (current_char == ':') { current_state = State.D10; }
                    else { Stop = true; }
                    break;
                case State.D29:
                    if (current_char == '1') { current_state = State.D30; }
                    else if (current_char == '2') { current_state = State.D30; }
                    else if (current_char == '3') { current_state = State.D30; }
                    else if (current_char == '4') { current_state = State.D30; }
                    else if (current_char == '5') { current_state = State.D30; }
                    else if (current_char == '6') { current_state = State.D30; }
                    else if (current_char == '7') { current_state = State.D30; }
                    else if (current_char == '8') { current_state = State.D30; }
                    else if (current_char == '9') { current_state = State.D30; }
                    else if (current_char == '0') { current_state = State.D290; }
                    else { Stop = true; }
                    break;
                case State.D30:
                    if (Char.IsDigit(current_char)) { current_state = State.D30; }
                    else if (current_char == '.') { current_state = State.D32; }
                    else if (current_char == ' ') { current_state = State.D18; }
                    else if (current_char == 'E') { current_state = State.D34; }
                    else { Stop = true; }
                    break;
                case State.D31:
                    if (current_char == '.') { current_state = State.D32; }
                    else if (current_char == ' ') { current_state = State.D18; }
                    else { Stop = true; }
                    break;
                case State.D32:
                    if (Char.IsDigit(current_char)) { current_state = State.D33; }
                    else { Stop = true; }
                    break;
                case State.D33:
                    if (Char.IsDigit(current_char)) { current_state = State.D33; }
                    else if (current_char == 'E') { current_state = State.D34; }
                    else if (current_char == ' ') { current_state = State.D18; }
										/*
                    else if (current_char == '1') { current_state = State.D330; }
                    else if (current_char == '2') { current_state = State.D330; }
                    else if (current_char == '3') { current_state = State.D330; }
                    else if (current_char == '4') { current_state = State.D330; }
                    else if (current_char == '5') { current_state = State.D330; }
                    else if (current_char == '6') { current_state = State.D330; }
                    else if (current_char == '7') { current_state = State.D330; }
                    else if (current_char == '8') { current_state = State.D330; }
                    else if (current_char == '9') { current_state = State.D330; }
										*/
                    else { Stop = true; }
                    break;
                case State.D34:
                    if (current_char == '1') { current_state = State.D35; }
                    else if (current_char == '2') { current_state = State.D35; }
                    else if (current_char == '3') { current_state = State.D35; }
                    else if (current_char == '4') { current_state = State.D35; }
                    else if (current_char == '5') { current_state = State.D35; }
                    else if (current_char == '6') { current_state = State.D35; }
                    else if (current_char == '7') { current_state = State.D35; }
                    else if (current_char == '8') { current_state = State.D35; }
                    else if (current_char == '9') { current_state = State.D35; }
                    else { Stop = true; }
                    break;
                case State.D35:
                    if (Char.IsDigit(current_char)) { current_state = State.D35; }
                    else if (current_char == ' ') { current_state = State.D18; }
                    else { Stop = true; }
                    break;
                case State.D18:
                    if (current_char == ' ') { current_state = State.D18; }
                    else if (current_char == 'E') { current_state = State.G1; }
                    else { Stop = true; }
                    break;
                case State.D15:
                    if (current_char == '1') { current_state = State.D16; }
                    else if (current_char == '2') { current_state = State.D16; }
                    else if (current_char == '3') { current_state = State.D16; }
                    else if (current_char == '4') { current_state = State.D16; }
                    else if (current_char == '5') { current_state = State.D16; }
                    else if (current_char == '6') { current_state = State.D16; }
                    else if (current_char == '7') { current_state = State.D16; }
                    else if (current_char == '8') { current_state = State.D16; }
                    else if (current_char == '9') { current_state = State.D16; }
                    else { Stop = true; }
                    break;
                case State.D16:
                    if (Char.IsDigit(current_char)) { current_state = State.D16; }
                    else if (current_char == ']') { current_state = State.D18; }
                    else { Stop = true; }
                    break;
                case State.D17:
                    if (current_char == ']') { current_state = State.D18; }
                    else { Stop = true; }
                    break;
                case State.D40:
                    if (Char.IsLetter(current_char)) { current_state = State.D40; }
                    else if (current_char == '_') { current_state = State.D40; }
                    else if (current_char == '1') { current_state = State.D40; }
                    else if (current_char == '2') { current_state = State.D40; }
                    else if (current_char == '3') { current_state = State.D40; }
                    else if (current_char == '4') { current_state = State.D40; }
                    else if (current_char == '5') { current_state = State.D40; }
                    else if (current_char == '6') { current_state = State.D40; }
                    else if (current_char == '7') { current_state = State.D40; }
                    else if (current_char == '8') { current_state = State.D40; }
                    else if (current_char == '9') { current_state = State.D40; }
                    else if (current_char == ']') { current_state = State.D18; }
                    else { Stop = true; }
                    break;
                case State.D290:
                    if (current_char == '.') { current_state = State.D32; }
                    else { Stop = true; }
                    break;
                case State.H1:
                    if (current_char == 'S') { current_state = State.H2; }
                    else { Stop = true; }
                    break;
                case State.H2:
                    if (current_char == 'I') { current_state = State.S1; }
                    else { Stop = true; }
                    break;
            }
        }


        if (Stop || ((pointer != line.Length - 1) && !Stop))
        {
            error = new Error() { Pointer = pointer, Comment = $"Состояние: {current_state}" };
            Console.WriteLine($"Остановка на {pointer}, длина строки {line.Length}");
        }

        return new Report()
        {
            Error = error,
            StatesLog = StateLog,
            InputLine = line
        };

    }
}


public struct Report
{
    public Error? Error { get; init; }
    public List<State> StatesLog { get; init; }
    public int Pointer { get; init; }
    public string InputLine { get; init; }
    public bool IsSuccess() => Error == null;
}



public struct Error
{
    public int Pointer { get; init; }
    public string Comment { get; init; }
}


public enum State
{
    S, S1, S2, S3, A1, A2, A4, A21, C, A30, A31, A32, C1, C2, C02, C03, C3, C04, C4, C5, C05, C005, C6, C06, C006, C7, C8, C9, D1, D2, D3, D4, C0, C10, C11, C21, C22, C23, C24, C30, C31, C32, C33, C40, C41, C42, C43, C44, C48, C45, C46, C47, B1, B2, B3, B4, B5, B6, B7, B8, D5, D6, D7, D10, D11, D12, D14, D13, D20, D21, D22, D23, G1, G2, G3, F, D24, D25, D26, D27, D28, D29, D30, D31, D32, D33, D34, D35, D18, D15, D16, D17, D40, D290, D330, H1, H2

}
